#!/usr/bin/python3
# coding: utf-8

import argparse
import socket
import sys
import threading

class proxy():
    def __init__(self, host='127.0.0.1', listening_port=7777, max_connection=5, buffer_size=8192):
        self.host = host
        self.listening_port = listening_port
        self.max_connection = max_connection
        self.buffer_size = buffer_size
        self.server_socket = None

    def __start__(self):
        try:
            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server_socket.bind((self.host, self.listening_port))
            self.server_socket.listen(self.max_connection)
            print('[+] Initializing sockets...')
            print('[+] Done')
            print('[+] Socket binded successfully.')
            print('[+] Server started successfully [' + str(self.listening_port) + '].')
        except Exception as e:
            print('[+] Unable to initialize socket.')
            sys.exit(2)

        while True:
            try:
                connection, address = self.server_socket.accept()
                data = connection.recv(self.buffer_size)
                my_thread = threading.Thread(target=self.__connection_string__, args=(connection, data, address))
                my_thread.start()
            except KeyboardInterrupt:
                self.server_socket.close()
                print('[+] Proxy server shutting down...')
                print('[+] See you later !')
                sys.exit(1)
        self.server_socket.close()

    def __connection_string__(self, connection, data, address):
        try:
            first_line = str(data).split('\n')[0]
            url = first_line.split(' ')[1]
            http_pos = url.find('://')

            if (http_pos == -1):
                tmp = url
            else:
                tmp = url[(http_pos + 3):]

            port_pos = tmp.find(':')
            webserver_pos = tmp.find('/')

            if webserver_pos == -1:
                webserver_pos = len(tmp)

            webserver = ''
            port = -1

            if (port_pos == -1 or webserver_pos < port_pos):
                port = 80
                webserver = tmp[:webserver_pos]
            else:
                port = int((tmp[(port_pos + 1):])[:webserver_pos - port_pos - 1])
                webserver = tmp[:port_pos]
            self.__proxy_server__(webserver, port, connection, address, data)
        except Exception as e:
            pass

    def __proxy_server__(self, webserver, port, connection, address, data):
        try:
            self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client_socket.settimeout(10)
            self.client_socket.connect((webserver, port))
            self.client_socket.send(data)

            while 1:
                reply = self.client_socket.recv(self.buffer_size)

                if (len(reply) > 0):
                    connection.send(reply)

                    dar = float(len(reply))
                    dar = float(dar / 1024)
                    dar = "%.3s" % (str(dar))
                    dar = "%s KB" % (dar)
                    print("[+] Request done: %s => %s <=" % (str(address[0]), str(dar)))
                else:
                    break

            self.client_socket.close()
            connection.close()
        except socket.error as error:
            self.client_socket.close()
            connection.close()
            sys.exit(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Python proxy')
    parser.add_argument('host', type=str, help='Host.')
    parser.add_argument('listening_port', type=int, help='Listening port.')
    args = parser.parse_args()

    my_proxy = proxy(args.host, args.listening_port)
    my_proxy.__start__()
